﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class WebCameraTest : MonoBehaviour
{
    public Text tips;

    void Start()
    {
        StringBuilder cameras = new StringBuilder();
        WebCamDevice[] devices = WebCamTexture.devices;
        for (int i = 0; i < devices.Length; i++)
        {
            cameras.AppendLine(devices[i].name);
        }
        tips.text = cameras.ToString();
    }
}
