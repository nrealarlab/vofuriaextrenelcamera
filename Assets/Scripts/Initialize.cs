﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using Vuforia;

public class Initialize : MonoBehaviour
{
    struct FileDriverUserData
    {
        public string sequenceDirectoryAbsolutePath;
    };

    private static string filePath;
    private static FileDriverUserData userData;
    private static IntPtr userDataPtr = IntPtr.Zero;

    // Start is called before the first frame update
    void Awake()
    {
        filePath = Application.streamingAssetsPath.Replace("/", "\\"); // get the absolute   path to the StreamingAssets folder

        userData = new FileDriverUserData()
        {
            sequenceDirectoryAbsolutePath = filePath
        };
        userDataPtr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(FileDriverUserData)));
        Marshal.StructureToPtr(userData, userDataPtr, false);

        VuforiaUnity.SetDriverLibrary("libFileDriver.so"); // Android
        VuforiaRuntime.Instance.InitVuforia();
    }
}
